## Name
sami.keilani@mnd.thm.de

## Beschreibung
Meine Idee ist es, eine Webseite aufzubauen, die mich optimal repräsentiert. Das Design, der Inhalt und die benutzerfreundliche Struktur der Website sollen es zukünftigen Nutzern leicht machen, meine Fähiigkeiten zu verstehen und sich mit mir zu identifizieren.




## Ausführung

Installation der Entwicklungsumgebung
Installiere Flutter auf Deinem Computer. Folge der offiziellen Installationsanleitung auf der Flutter Webseite. Stelle sicher, dass die Flutter-Umgebung richtig konfiguriert ist, indem Du den Befehl flutter doctor im Terminal ausführst. Korrigiere eventuelle Fehler oder fehlende Komponenten, bis der Befehl keine relevanten Fehler mehr anzeigt.


Einrichtung des Git-Repositories
Erstelle ein neues Repository auf GitHub, GitLab oder einer ähnlichen Plattform. Der Name des Repositories muss portfolio_ sein, wobei  durch Deine Matrikelnummer zu ersetzen ist.
Lade mich per E-Mail in Dein Git-Repository ein. Verwende dazu die E-Mail-Adresse des Dozenten.


Erstellung einer README-Datei und Initialisierung des Flutter-Projekts
Füge im Hauptverzeichnis Deines Repositories eine README.md Datei hinzu. Diese sollte eine kurze Beschreibung des Projektes sowie Anweisungen zur Installation und Ausführung enthalten.
Initialisiere innerhalb des Repositories ein neues Flutter-Projekt mit dem Namen portfolio_ . Verwende dazu den Befehl:
flutter create portfolio_
